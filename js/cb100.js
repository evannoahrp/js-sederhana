let cb100 = {
  nama: "Honda CB 100",
  warna: "hitam",
  dimensi: {
    panjang: "1,955 mm",
    lebar: "840 mm",
    tinggi: "1,095 mm",
    beratKosong: "95 kg",
  },
  mesin: "4-tak OHC",
  tenagaMaksimal: "11,5 hp",
  kecepatanMaksimal: "110,0 km/jam",
  kapasitasBahanBakar: "4.5 l",
  transmisi: "5 kecepatan",
};
console.log(cb100);
