let kendaraan = [
  {
    jenis: "mobil",
    tipe: "minivan",
    warna: "putih",
    kapasitas: 7,
  },
  {
    jenis: "mobil",
    tipe: "station wagon",
    warna: "hitam",
    kapasitas: 5,
  },
  {
    jenis: "sepeda motor",
    tipe: "bebek",
    warna: "merah",
    kapasitas: 2,
  },
  {
    jenis: "sepeda motor",
    tipe: "matic",
    warna: "hitam",
    kapasitas: 2,
  },
  {
    jenis: "mobil",
    tipe: "sedan",
    warna: "putih",
    kapasitas: 5,
  },
];
console.log(kendaraan);
